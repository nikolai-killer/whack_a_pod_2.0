package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/", health)
	r.GET("/healthz", health)

	r.POST("/crash", func(ctx *gin.Context) {
		os.Exit(1)
	})

	r.Run() // listen and serve on 0.0.0.0:8080
}

func health(c *gin.Context) {
	c.String(http.StatusOK, "OK")
}
