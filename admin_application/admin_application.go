package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const namespace = "default"

func main() {

	config, err := rest.InClusterConfig()
	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	r := gin.Default()
	r.Use(CORSMiddleware())

	r.GET("/pods", func(c *gin.Context) {
		getPods(c, clientset)
	})

	r.GET("/status/:podname", func(c *gin.Context) {
		podname := c.Param("podname")
		getPodStatus(c, clientset, podname)
	})

	r.POST("/crash/:podname", func(c *gin.Context) {
		podname := c.Param("podname")
		crashPod(c, clientset, podname)
	})

	r.Run() // listen and serve on 0.0.0.0:8080
}

func crashPod(c *gin.Context, clientset *kubernetes.Clientset, podname string) {
	podIP, err := getPodIp(clientset, podname)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Pod not found"})
		fmt.Println(err.Error())
		return
	}
	fmt.Println("Pod IP:", podIP)

	http.Post(fmt.Sprintf("http://%s:8080/crash", podIP), "text/plain", nil)
	c.String(http.StatusOK, "CRASHED")
}

func getPodStatus(c *gin.Context, clientset *kubernetes.Clientset, podname string) {
	podIP, err := getPodIp(clientset, podname)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Pod not found"})
		fmt.Println(err.Error())
		return
	}

	resp, err := http.Get(fmt.Sprintf("http://%s:8080", podIP))

	if err != nil || resp.StatusCode != 200 {
		c.String(http.StatusOK, "DOWN")
		return
	}

	defer resp.Body.Close()
	c.String(http.StatusOK, "OK")
}

func getPodIp(clientset *kubernetes.Clientset, podname string) (string, error) {
	pod, err := clientset.CoreV1().Pods(namespace).Get(context.TODO(), podname, metav1.GetOptions{})
	if err != nil {
		return "", err
	}

	return pod.Status.PodIP, nil
}

func getPods(c *gin.Context, clientset *kubernetes.Clientset) {
	pods, err := clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{LabelSelector: "app=whack-api"})
	if err != nil {
		panic(err.Error())
	}

	podNames := make([]string, len(pods.Items))
	for i, pod := range pods.Items {
		podNames[i] = pod.GetName()
	}

	c.JSON(http.StatusOK, gin.H{"podNames": podNames})
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}
