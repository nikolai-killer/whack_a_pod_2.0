import 'package:whack_game/main.dart';
import 'package:whack_game/pod_state.dart';
import 'package:http/http.dart' as http;

class Pod {
  final String podname;
  PodState podState;
  bool refetchState = true;

  Pod({
    required this.podname,
    this.podState = PodState.ok
  }) {
    updateState();
  }

  Future<void> crashPod() async {
    await http.post(Uri.parse('$baseURI/admin/crash/$podname'));
    refetchState = true;
  }

  Future<PodState> updateState() async {
    if(!refetchState) return podState;
    final response = await http.get(Uri.parse('$baseURI/admin/status/$podname'));
    if(response.statusCode == 200 && response.body == "OK"){
      podState = PodState.ok;
      refetchState = false;
    } else {
      podState = PodState.down;
    }
    return podState;
  }
}