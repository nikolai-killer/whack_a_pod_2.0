import 'package:flutter/material.dart';
import 'package:whack_game/pod.dart';
import 'package:whack_game/pod_state.dart';
import 'package:http/http.dart' as http;
import 'package:whack_game/main.dart';


class PodWidget extends StatelessWidget {
  final Pod pod;
  const PodWidget({super.key, required this.pod});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 20,
      width: 20,
      child: FilledButton(
        onPressed: pod.crashPod, 
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith((_) => pod.podState == PodState.ok ? Colors.green :  Colors.red),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5)
          ))),
        child: Text(pod.podname, style: const TextStyle(fontSize: 10),),
      ),
    );
  }
}