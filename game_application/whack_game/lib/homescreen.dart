import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:whack_game/main.dart';
import 'package:whack_game/pod.dart';
import 'package:whack_game/pod_state.dart';
import 'package:whack_game/pod_widget.dart';
//import 'package:whack_game/state.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Timer? refreshTimer;
  //HomeScreenState _state = HomeScreenState.waiting;
  final List<Pod> _podList = List.empty(growable: true);
  final int oneRowAmount = 4;

  /*void startGame() {
    setState(() {
      _state = HomeScreenState.running;
    });
  }

  void endGame() {
    setState(() {
      _state = HomeScreenState.waiting;
    });
  }*/

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Column(
          children: [
            SizedBox(height: 15,),
            Text("Whack-A-Pod", style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
          ],
        ),
        Flexible(
          child: SizedBox(
            width: 600,
            child: GridView.count(
              shrinkWrap: true,
              mainAxisSpacing: 30,
              crossAxisSpacing: 30,
              childAspectRatio: 1,
              crossAxisCount: oneRowAmount,
              children: List.generate(_podList.length, (index) {
                return PodWidget(pod: _podList[index]);
              }),
            ),
          ),
        ),
        const Column(
          children: [
            /*SizedBox(
              height: 70,
              width: 300,
              child: FloatingActionButton(
                backgroundColor: Colors.blue,
                onPressed: () {
                  if(_state == HomeScreenState.waiting) {
                    startGame();
                  } else {
                    endGame();
                  }
                }, 
                child: Text(_state == HomeScreenState.waiting? "Start": "End", style: const TextStyle(fontWeight: FontWeight.bold),)
              ),
            ),*/
            SizedBox(height: 30,),
          ],
        )
        
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    fetchPods();
  }

  void fetchPods() async {
    final response = await http.get(Uri.parse('$baseURI/admin/pods'));
    if(response.statusCode == 200) {
      Map<String,  dynamic> data = jsonDecode(response.body) as Map<String, dynamic>;
      Iterable<Pod> podnamelist = (data["podNames"] as List<dynamic>).map((dynamic name) => Pod(podname: name as String));
      setState(() {
        _podList.clear();
        _podList.addAll(podnamelist);
      });
      refreshTimer = Timer.periodic(const Duration(seconds: 1), (Timer t) => updateStates());
    }
  }

  void updateStates() async {
    List<Future<PodState>> futures = [for (Pod pod in _podList) pod.updateState()];
    await Future.wait(futures);    
    setState(() {});
  }

  @override
  void dispose() {
    refreshTimer?.cancel();
    super.dispose();
  }
}