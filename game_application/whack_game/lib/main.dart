import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:whack_game/homescreen.dart';

String baseURI = Uri.base.toString();

void main() {
  // delete if 
  if(kDebugMode && baseURI.startsWith("http://localhost:")) {
    baseURI = "http://ec2-3-67-38-159.eu-central-1.compute.amazonaws.com";
  } 
  Uri url = Uri.parse(baseURI);
  if (url.path.isNotEmpty){
    Uri newUrl = url.replace(path: '');
    baseURI = newUrl.toString();
  }
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
          child: HomeScreen(),
        ),
      ),
    );
  }
}
